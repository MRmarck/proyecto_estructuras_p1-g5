/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication1;

import Views.SimulationView;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.scene.Node;

/**
 *
 * @author miguel_parra
 */
public class FXMLDocumentController implements Initializable {

    
    @FXML
    private Button btnAddPlayer;

    @FXML
    private Button btnRemovePlayer;

    @FXML
    private Label lblPlayer;  
    
    private String direction = "Horario";
    @FXML
    private Button btnPlay;
    

    @FXML
    private Button btnAntihorario;

    @FXML
    private Button btnHorario;
    
    
    @FXML
    void changeRotationAntihorario(ActionEvent event) {
         this.direction = "Antihorario";
    }

    @FXML
    void changeRotationHorario(ActionEvent event) {
        this.direction = "Horario";
    }
    
    @FXML
    void startGame(ActionEvent event) {
        
            int nPlayer = Integer.parseInt(lblPlayer.getText());
            int nUntouchables = 0;
            SimulationView root = new SimulationView(nPlayer, nUntouchables,direction);

            Stage stage = (Stage) lblPlayer.getScene().getWindow();
            stage.getScene().setRoot(root);
            stage.setWidth(895);
            stage.setHeight(585);
            stage.setResizable(false);
        
    }
    
    @FXML
    void changeSceneToSettings(ActionEvent event) throws IOException  {
        
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument2.fxml"));
        
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void addAPlayer(ActionEvent event) {
        String actualText = lblPlayer.getText();
        int actualPlayer = Integer.parseInt(actualText);
        if(actualPlayer<10){
            String aPlayer = String.valueOf(actualPlayer + 1);
            lblPlayer.setText(aPlayer);
        }
    }

    @FXML
    void removeAPlayer(ActionEvent event) {
        String actualText = lblPlayer.getText();
        int actualPlayer = Integer.parseInt(actualText);
        if(actualPlayer>2){
            String aPlayer = String.valueOf(actualPlayer - 1);
            lblPlayer.setText(aPlayer);
        }
    } 

    
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //to do
    }

}
