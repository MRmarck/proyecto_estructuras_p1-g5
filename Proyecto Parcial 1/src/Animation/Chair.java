/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animation;

import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 *
 * @author Marck
 */
public class Chair extends VBox {

    private ImageView figure;

    public Chair() {
        this.figure = new ImageView(assignFigure());
        this.getChildren().add(figure);
    }

    /**
     * *
     * Metodo para asignar una figura a cada silla
     *
     * @return
     */
    private Image assignFigure() {
        File path = new File("./resources/images/chairs");
        int pathSize = path.list().length;

        SecureRandom rand;
        try {
            rand = SecureRandom.getInstance("SHA1PRNG");
            int r = rand.nextInt(pathSize);
            return new Image("file:./resources/images/chairs/chair" + r + ".png", 60, 60, true, true);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Chair.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }
}
