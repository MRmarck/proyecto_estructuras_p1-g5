package Animation;

import TDAs.ArrayList;
import TDAs.List;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Slider;
import javafx.scene.layout.Pane;

public class MovementSimulation extends Thread {

    private final ChoiceBox chbxMov;
    private final Slider sldSpeed;
    private final Pane ctnCentro;
    private final ArrayList<Player> listaJugadores;
    private boolean dancing;
    private int numbPlayers;
    private final ArrayList<Double> possibleAngles;

    public MovementSimulation(Pane ctnCentro, List<Player> listaJugadores, ChoiceBox chbxMov, Slider sldSpeed) {
        this.listaJugadores = (ArrayList<Player>) listaJugadores;
        this.ctnCentro = ctnCentro;
        this.chbxMov = chbxMov;
        this.sldSpeed = sldSpeed;
        this.dancing = false;
        this.numbPlayers = 0;
        this.possibleAngles = new ArrayList<>();
    }

    @Override
    public void run() {
        //Pequeño tiempo de espera para permitirle iniciaizacion de otras variables
        try {
            sleep(500);
        } catch (InterruptedException e) {
            System.out.println(e);
            Thread.currentThread().interrupt();

        }

        String dirRotation;
        long speed;

        while (ctnCentro.getScene().getWindow().isShowing()) {
            //Pequeño tiempo de espera para permitirle al ciclo infinito ejecutar el condicional siguiente
            try {
                sleep(100);
            } catch (InterruptedException e) {
                System.out.println(e);
                Thread.currentThread().interrupt();

            }

            if (dancing) {
                try {
                    for (int i = 0; i < listaJugadores.size(); i++) {
                        dirRotation = chbxMov.getValue().toString();
                        speed = (long) (sldSpeed.valueProperty().getValue() * 1000);

                        rotate(dirRotation);
                        Thread.sleep(speed);
                    }
                } catch (InterruptedException e) {
                    System.out.println(e);
                    Thread.currentThread().interrupt();

                }
            } else if (numbPlayers != listaJugadores.size()) {
                setPossibleAngles();
                numbPlayers = listaJugadores.size();
            }
        }
    }



    /**
     * *
     * Metodo para llenar "possibleAngles" con las coordenadas posibles donde
     * puedan colocarse los jugadores
     */
    private void setPossibleAngles() {
        double angle;
        double xOffset;
        double yOffset;
        double x;
        double y;
        int avaibleSpaces = listaJugadores.size() * 3;

        possibleAngles.clear();
        for (int i = 0; i < avaibleSpaces; i++) {
            angle = 2 * i * Math.PI / avaibleSpaces;
            xOffset = 170 * Math.cos(angle);
            yOffset = 170 * Math.sin(angle);
            x = 200 + xOffset;
            y = 150 + yOffset;
            possibleAngles.addLast(x);
            possibleAngles.addLast(y);
        }
    }

    /**
     * *
     * Metodo para configurar las coordenadas de cada jugador segun los valores
     * dentro de "possibleAngles", ademas modifica dicha variable para añadir la
     * ilucion de una animacion
     *
     * @param direction :sentido de la rotacion
     */
    public void rotate(String direction) {

        if (direction.equals("Horario")) {
            possibleAngles.addLast(possibleAngles.removeFirst());
            possibleAngles.addLast(possibleAngles.removeFirst());

        } else {
            possibleAngles.addFirst(possibleAngles.removeLast());
            possibleAngles.addFirst(possibleAngles.removeLast());
        }

        int counter = 0;
        for(int i=0; i< listaJugadores.size(); i++) {
            Player p = listaJugadores.get(i);
            p.setLayoutX(possibleAngles.get(counter));
            p.setLayoutY(possibleAngles.get(counter + 1));
            counter = counter + 6;
        }

    }

    ////////////////    GETTERS Y SETTERS   ////////////////////////////
    
    public void setDancing(boolean dancing) {
        this.dancing = dancing;
    }

    public boolean isDancing() {
        return dancing;
    }
    
    
}
