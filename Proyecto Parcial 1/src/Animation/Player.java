/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animation;

import TDAs.ArrayList;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 *
 * @author Marck
 */
public class Player extends VBox {

    public static ArrayList<String> Names;
    private boolean isUntouchable;
    private Label lblName;
    private ImageView figure;
    private SecureRandom rand;
    private int rann = 0;
    /**
     * Cada jugador se compone de una ImageView para mostar alguna figura y de
     * un Label para reconocerlo con un texto
     */
    public Player(boolean isUntouchable) {
        Names = loadNames();
        this.isUntouchable = isUntouchable;

        String name = createName();
        this.lblName = new Label(name);
        this.figure = new ImageView(assignFigure());

        this.getChildren().add(figure);

        this.getChildren().add(new Label(name));

    }

    /**
     * Metodo para crear un nombre aleatorio para la variable LblName
     *
     * @return nombre aleatorio
     */
    private String createName() {
        
        int pathSize = Names.size();
        try {
            rand = SecureRandom.getInstance("SHA1PRNG");
            
            int r = rand.nextInt(pathSize);
            rann= r;
            return Names.get(r);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * *
     * Metodo para asignar una figura a cada niño
     *
     * @return
     */
    private Image assignFigure() {
        File path = new File("./resources/images/child");
        int pathSize = path.list().length;

        try {
            rand = SecureRandom.getInstance("SHA1PRNG");
            int r = rann;
            return new Image("file:./resources/images/child/kid" + r + ".png", 50, 50, true, true);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    private ArrayList<String> loadNames() {
        FileReader fr = null;
        BufferedReader br = null;
        if (Names == null) {
            ArrayList<String> names = new ArrayList<>();
            File archivo = new File("./resources/names.txt");
            try {
                fr = new FileReader(archivo);
                br = new BufferedReader(fr);

                String line = "";
                while (line != null) {
                    if (!line.equals("")) {
                        names.addFirst(line);
                    }
                    line = br.readLine();
                }

            } catch (FileNotFoundException e) {
                System.out.println(e);
            } catch (IOException e) {
                System.out.println(e);
            } finally {
                try {
                    if (br != null) 
                        br.close();
                    if (fr != null) 
                        fr.close();
                    
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }catch (NullPointerException e){
                    System.out.println(e.getMessage());
                    
                }
            }
            return names;
        }
        return Player.Names;
    }

    public boolean isIsUntouchable() {
        return isUntouchable;
    }

    public void setIsUntouchable(boolean isUntouchable) {
        this.isUntouchable = isUntouchable;
    }

    public Label getLblName() {
        return lblName;
    }

}
