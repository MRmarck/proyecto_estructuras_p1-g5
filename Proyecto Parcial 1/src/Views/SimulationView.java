package Views;

import Animation.Chair;
import Animation.MovementSimulation;
import Animation.Player;
import Posicion.Posiciones;
import TDAs.ArrayList;
import TDAs.DobleEnlaceCircular;
import TDAs.LinkedList;
import java.io.File;
import static java.lang.Thread.sleep;
import java.util.Iterator;
import java.util.Random;
import java.util.Stack;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

/**
 *
 * @author Marck
 */
public class SimulationView extends BorderPane{
    private final int nPlayers;
    private final int nUntouchables;
    private final String direction;
    private BorderPane root;
    private VBox ctnDerecha;
    private VBox ctnIzquierda;
    private VBox ctnSuperior;
    private Pane ctnCentro;
    private boolean flag = false;
    final String Nombre = "Audio.mp3";
    private ArrayList<Player> listaJugadores;
    private Player eliminada;
    private DobleEnlaceCircular<Posiciones> posiciones;

    
    private LinkedList<Player> listaEliminados;

    public SimulationView(int nPlayers, int nUntouchables, String direction) {
        this.nPlayers = nPlayers;
        this.nUntouchables = nUntouchables;
        this.direction = direction;
        
        //Initial_settings();
        llenarArray();
        mount_elements();
    }


    private void mount_elements() {
        
        /////Llenar Parte Superior//////
        Label lblSuperior = new Label("");
        
        setBackground("src/center.png");
        
        //Boton para cambiar en tiempo real el sentido de la animacion
        ChoiceBox chbxMov = new ChoiceBox(FXCollections.observableArrayList("Horario", "Antihorario"));
            chbxMov.setValue(direction);
            chbxMov.setStyle("-fx-font-size: 10");
        //Slider para controlar la velocidad de la animacion en tiempo real
        Slider sldSpeed = new Slider(0, 1, 0.5);
        
        /*ctnSuperior = new VBox();
        ctnSuperior.setAlignment(Pos.CENTER);
        ctnSuperior.setMinSize(600, 10);
        ctnSuperior.getChildren().addAll(LblSuperior, chbxMov, sldSpeed);
        this.setTop(ctnSuperior);*/
        ctnSuperior = new VBox();
        File fileT = new File("src/top.png");
        BackgroundImage myBIT= new BackgroundImage(new Image(fileT.toURI().toString(),895,50,false,true),
        BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
          BackgroundSize.DEFAULT);
        //then you set to your node
        ctnSuperior.setBackground(new Background(myBIT));
            
        ctnSuperior.setAlignment(Pos.CENTER);
        ctnSuperior.setMinSize(600, 10);
        ctnSuperior.getChildren().addAll(lblSuperior, chbxMov, sldSpeed);
        
        this.setTop(ctnSuperior);

        ////LLenar Parte Inferior///////
        Button bttnEmpezar = new Button("Play");
        Button bttnParar = new Button("Stop");
        HBox CtnBajo = new HBox();
        CtnBajo.setAlignment(Pos.CENTER);
        CtnBajo.getChildren().addAll(bttnEmpezar, bttnParar);
        CtnBajo.setMinSize(600, 30);
        this.setBottom(CtnBajo);

        ////Llenar Parte Derecha////////
   
        Label h=new Label("                  ");
        ctnDerecha = new VBox();
        
        File fileR = new File("src/right.png");
        BackgroundImage myBIR= new BackgroundImage(new Image(fileR.toURI().toString(),56,370,false,true),
        BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
          BackgroundSize.DEFAULT);
        //then you set to your node
        ctnDerecha.setBackground(new Background(myBIR));
        
        ctnDerecha.setAlignment(Pos.CENTER);
        ctnDerecha.setMaxSize(80, 360);
        ctnDerecha.getChildren().add(h);
            LlenarLadoDerecho();
        this.setRight(ctnDerecha);

        ////Llenar Parte Centrar///////
        ctnCentro = new Pane();
        LlenarCentroSillas();
        LlenarCentroPersonas();
        ctnCentro.setMaxSize(440, 320);
        this.setCenter(ctnCentro);

        ///Llenar Parte Izquierda//////
        ctnIzquierda = new VBox();
        
        File fileL = new File("src/left.png");
        BackgroundImage myBIL= new BackgroundImage(new Image(fileL.toURI().toString(),60,290,false,true),
        BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
          BackgroundSize.DEFAULT);
        //then you set to your node
        ctnIzquierda.setBackground(new Background(myBIL));
        
        ctnIzquierda.setAlignment(Pos.CENTER);
        ctnIzquierda.setMaxSize(80, 360);
        ctnIzquierda.getChildren().add(new Label("                           "));
        listaEliminados=new LinkedList<>();
        this.setLeft(ctnIzquierda);
        
        
        ///////////Code parar musica/////////
        File archivo = new File(Nombre);
        Media audio = new Media(archivo.toURI().toString());
        MediaPlayer reproductor = new MediaPlayer(audio);

        //Instacionacion de la clase para simular el movimiento
        MovementSimulation move = new MovementSimulation(ctnCentro, listaJugadores, chbxMov, sldSpeed);
        move.start();
        
        ////////BOTON EMPEZAR/////////
        bttnEmpezar.setOnAction((ActionEvent e) -> {
            reproductor.play();
            move.setDancing(true);
            flag = true;
            setBackground("src/centro2.png");
        });

        ///////BOTON PARAR//////////
        bttnParar.setOnAction((ActionEvent e) -> {
            if(flag){
                // tiempo de espera aleatorio para revelar al jugador eliminado
                Random rand = new Random();
                int ramdomWait = rand.nextInt(2);
                try {
                    reproductor.pause();
                    sleep(ramdomWait);
                } catch (InterruptedException k) {
                    System.out.println(k);
                    Thread.currentThread().interrupt();
                }   
                
                move.setDancing(false);
                AcualizaDerecha(ctnDerecha);
                ActualizarIzquierda(ctnIzquierda);

                System.out.println("intocables "+ nUntouchables);
                if ( nUntouchables != 0 && listaJugadores.size() == nUntouchables) {
                    ActualizaCentro();
                    ganador();
                }else if ( nUntouchables == 0 && listaJugadores.size() == 1) {
                    ganador();
                }else{
                    ActualizaCentro();
                    Alert alert = new Alert(Alert.AlertType.NONE, "Persona eliminada es : " + eliminada.getLblName().getText(), ButtonType.OK);
                    alert.showAndWait();
                }
                setBackground("src/center.png");
                
                //reset de la direccion de baile
                if( chbxMov.getValue().toString().equals("Horario")){
                    chbxMov.setValue("Antihorario");
                }else{
                    chbxMov.setValue("Horario");
                }
            }
            flag = false;
        
        } );
    }
    
    
    public final void llenarArray() {
        listaJugadores = new ArrayList<>();
        for(int i=0; i < nPlayers; i++){
            listaJugadores.addLast(new Player(false));
        }
        for(int i=0; i < nUntouchables; i++){
            listaJugadores.addLast(new Player(true));

        }
    }

    public void setBackground(String s){
        File fileM = new File(s);
        BackgroundImage myBIM= new BackgroundImage(new Image(fileM.toURI().toString(),900,535,false,true),
        BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
          BackgroundSize.DEFAULT);
        //then you set to your node
        this.setBackground(new Background(myBIM));
    }
    
    public void ActualizaCentro() {
        ctnCentro.getChildren().clear();
        LlenarCentroPersonas();
        LlenarCentroSillas();

    }

    public void Angulo() {
        float angulo = 360f / listaJugadores.size();
        posiciones = new DobleEnlaceCircular<>();
        float m = angulo;
        for (int w = 0; w < listaJugadores.size(); w++) {
            posiciones.addLast(new Posiciones(100, m));
            m = angulo + m;
        }
    }
    

    
    public void LlenarCentroSillas() {
        ArrayList<Double> angles = new ArrayList<>();
        double angle;
        double xOffset;
        double yOffset;
        double x;
        double y;
        int nChairs = (listaJugadores.size() -1);
        int avaibleSpaces = nChairs * 3;

        angles.clear();
        for (int i = 0; i < avaibleSpaces; i++) {
            angle = 2 * i * Math.PI / avaibleSpaces;
            xOffset = ((nChairs -1) *10) * Math.cos(angle);
            yOffset = ((nChairs -1) *10) * Math.sin(angle);
            x = 200 + xOffset;
            y = 150 + yOffset;
            angles.addLast(x);
            angles.addLast(y);
        }

        
        ////Posiciones de las sillas//////
        int counter = 0;
        for(int b=0; b< nChairs; b++) {
            Chair c = new Chair();
            c.setLayoutX(angles.get(counter));
            c.setLayoutY(angles.get(counter + 1));
            counter = counter + 6;
            ctnCentro.getChildren().add(c);
        }
    }

   public void  LlenarCentroPersonas(){
        Angulo();
        Stack<Player> pilaPersonas=new Stack();  
        Stack<Posiciones> PilaPosicion=new Stack();
        
        for (int c=0;c<posiciones.size();c++){
            pilaPersonas.push(listaJugadores.get(c));
            PilaPosicion.push(posiciones.get(c));
        }
        
        pilaPersonas.stream().map((p) -> {
            float a=PilaPosicion.pop().getAngulo();
            p.setAlignment(Pos.CENTER);
            p.setLayoutX( 170*Math.cos(a)+200);
            p.setLayoutY( 170*Math.sin(a)+150);
            return p;            
        }).forEachOrdered((p) -> {
            ctnCentro.getChildren().add(p);
        });
        
   }
    
    
    public void ganador() {
        
        String winners ="";
        System.out.println("LOS GANADORES SON: ");
        for(int i=0; i<listaJugadores.size();i++){
            winners += ", " + listaJugadores.get(i).getLblName().getText();
        }
        
        Alert alert = new Alert(Alert.AlertType.NONE,"GANADOR(ES): "+ winners, ButtonType.OK);
        alert.showAndWait();
        
        
        if (alert.getResult() == ButtonType.OK) {
            Stage stage = (Stage) ctnDerecha.getScene().getWindow();
            stage.close();
        }
        
    }

    ////////////////LOGICA DEL LADO DERECHO ////////////////////
    public void LlenarLadoDerecho() {
     
        Iterator<Player> it = listaJugadores.iterator();
        while (it.hasNext()) {
            Player a = it.next();
            ctnDerecha.getChildren().add(a.getLblName());

        }
    }

    public void AcualizaDerecha(VBox derecha) {
        
        derecha.getChildren().clear();
        derecha.getChildren().add(new Label("              "));
        Random rand = new Random();
        int randomPersona = rand.nextInt(listaJugadores.size() - nUntouchables);
        
        eliminada = listaJugadores.get(randomPersona);
        listaEliminados.addFirst(eliminada);
        listaJugadores.remove(randomPersona);
        LlenarLadoDerecho();
    }

    public void ActualizarIzquierda(VBox izquierda){
        izquierda.getChildren().clear();
        izquierda.getChildren().add(new Label("                   "));
        Iterator<Player> it = listaEliminados.iterator();
        while (it.hasNext()) {
            Player a = it.next();
            izquierda.getChildren().add(a);

        }
        
    }
    
    
    
    
    
    
    /////////////////////////////////////////////////////////
    public  BorderPane getRoot() {
        return root;
    }

    public void setRoot(BorderPane root) {
        this.root = root;
    }

    public VBox getctnDerecha() {
        return ctnDerecha;
    }

    public void setCtnDerecha(VBox ctnDerecha) {
        this.ctnDerecha = ctnDerecha;
    }

    public Pane getCtnCentro() {
        return ctnCentro;
    }

    public void setCtnCentro(Pane ctnCentro) {
        this.ctnCentro = ctnCentro;
    }

}

