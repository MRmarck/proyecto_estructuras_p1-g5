/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDAs;

/**
 *
 * @author LAPTOP
 */
public class NodeListDoble<E> {
    private E content;
    private NodeListDoble<E> next;
    private NodeListDoble<E> previous;

    public NodeListDoble(E content) {
        this.content = content;
        this.next=null;
        this.previous=null;
    }

    public NodeListDoble(E content, NodeListDoble<E> next, NodeListDoble<E> previous) {
        this.content = content;
        this.next = next;
        this.previous = previous;
    }

    public E getContent() {
        return content;
    }

    public void setContent(E content) {
        this.content = content;
    }

    public NodeListDoble<E> getNext() {
        return next;
    }

    public void setNext(NodeListDoble<E> next) {
        this.next = next;
    }

    public NodeListDoble<E> getPrevious() {
        return previous;
    }

    public void setPrevious(NodeListDoble<E> previous) {
        this.previous = previous;
    }
    
    



}
