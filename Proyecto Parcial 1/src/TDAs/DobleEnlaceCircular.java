/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDAs;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 *
 * @author LAPTOP
 * @param <E>
 */
public class DobleEnlaceCircular<E> implements List<E> {
    private NodeListDoble<E> header;
    private NodeListDoble<E> last;
    private int tamano;
    public DobleEnlaceCircular() {
        header = null;
        last = null;
        tamano = 0;
    }
    @Override
    public boolean addFirst(E e) {
        NodeListDoble<E> nuevo = new NodeListDoble<>(e);
        if (e != null) {
            if (isEmpty()) {
                nuevo.setNext(nuevo);
                nuevo.setPrevious(nuevo);
                setHeader(nuevo);
                setLast(nuevo);
                tamano++;
                return true;
            } else {
                nuevo.setNext(header);
                nuevo.setPrevious(last);
                setHeader(nuevo);
                last.setNext(nuevo);
                nuevo.getNext().setPrevious(nuevo);
                tamano++;
                return true;
            }
        } else {

            return false;
        }
    }
    @Override
    public boolean addLast(E e) {
        NodeListDoble<E> nuevo = new NodeListDoble<>(e);
        if (e == null) {
            return false;
        }
        if (isEmpty()) {
            nuevo.setNext(nuevo);
            nuevo.setPrevious(nuevo);
            setHeader(nuevo);
            setLast(nuevo);
            tamano++;
            return true;
        } else {
            last.setNext(nuevo);
            nuevo.setPrevious(last);
            header.setPrevious(nuevo);
            setLast(nuevo);
            last.setNext(header);

            tamano++;

            return true;
        }

    }
    @Override
    public E removeFirst() {
        if (isEmpty()) {
            return null;
        } else {
            E eliminado = getHeader().getContent();
            setHeader(header.getNext());
            header.setPrevious(last);
            last.setNext(header);
            tamano--;
            return eliminado;
        }
    }
    @Override
    public E removeLast() {
        if (isEmpty()) {
            return null;
        } else {
            E eliminado = getLast().getContent();
            header.setPrevious(last.getPrevious());
            setLast(last.getPrevious());
            last.setNext(header);
            return eliminado;
        }
    }
    @Override
    public int size() {
        return tamano;
    }
    @Override
    public boolean isEmpty() {
        return header == null;
    }
    @Override
    public void clear() {
        setHeader(null);
        setLast(null);
    }
    @Override
    public void add(int index, E element) {
        if (element != null && index <= tamano) {
                if (index == 0) {
                    addFirst(element);
                } else if (index == tamano) {
                    addLast(element);
                } else {
                    NodeListDoble<E> n;
                    int v = 0;
                    NodeListDoble<E> nuevo = new NodeListDoble<>(element);
                    for (n = header; n != null; n = n.getNext()) {
                        if (v == index - 1) {

                            nuevo.setNext(n.getNext());
                            n.getNext().setPrevious(nuevo);
                            nuevo.setPrevious(n);
                            n.setNext(nuevo);
                            break;
                        }
                        v++;
                    }
                }
        }
    }
    @Override
    public E remove(int index) {
        if (index < tamano) {
            NodeListDoble<E> n;
            int v = 0;
            if (index == 0) {
                removeFirst();
            }
            if (index == tamano - 1) {
                removeLast();
            }
            for (n = header; n != null; n = n.getNext()) {

                if (index == v) {
                    E o = n.getContent();
                    n.getNext().setPrevious(n.getPrevious());
                    n.getPrevious().setNext(n.getNext());
                    tamano--;
                    return o;
                }
                v++;
            }
        } else {
            return null;
        }
        return null;
    }
    @Override
    public E get(int index) {
        if (index < tamano) {
            NodeListDoble<E> n;
            int v = 0;
            for (n = header; n != null; n = n.getNext()) {
                if (v == index) {
                    return (n.getContent());
                }
                v++;
            }
        }
        return null;
    }
    @Override
    public E set(int index, E element) {
        if (index < tamano) {
            NodeListDoble<E> n;
            int v = 0;
            for (n = header; n != null; n = n.getNext()) {
                if (v == index) {
                    n.setContent(element);
                    return (n.getContent());
                }
                v++;
            }
        }
        return null;
    }
    @Override
    public Iterator<E> iterator() {

        Iterator<E> it = new Iterator<E>() {
            NodeListDoble<E> n = header;

            @Override
            public boolean hasNext() {
                return n != null;
            }

            @Override
            public E next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }

                E mandar = n.getContent();
                n = n.getNext();
                return mandar;

            }
        };
        return it;
    }
    //GETTERS AND SETTERS  
    public NodeListDoble<E> getHeader() {
        return header;
    }
    public void setHeader(NodeListDoble<E> header) {
        this.header = header;
    }
    public NodeListDoble<E> getLast() {
        return last;
    }
    public void setLast(NodeListDoble<E> last) {
        this.last = last;
    }
    public int getTamano() {
        return tamano;
    }
    public void setTamano(int tamano) {
        this.tamano = tamano;
    }
}
