package TDAs;

import java.util.Comparator;
import java.util.Iterator;

public interface List<E> {
    
   public Iterator<E> iterator();

    public boolean addFirst(E e); // inserta el elemento e al inicio
          
    public boolean addLast(E e); // inserta el elemento e al final

    public E removeFirst(); // remueve el elemento al inicio de la lista

    public E removeLast(); // remueve el elemento al final de la lista

    public int size();

    public boolean isEmpty();

    public void clear();
    
    public void add(int index, E element); // inserta element en la posición index

    public E remove(int index); // remueve y retorna el elemento en la posición index

    public E get(int index); // retorna el elemento ubicado en la posición index

    public E set(int index, E element); // setea el element en la posición index
    
   @Override
    public String toString(); // retorna una cadena de caracteres representando los elementos que la lista contien
    //Encuentra todos los objetos dentro de la lista
    //Que comparte un criterio con element
    //El cumplimiento del criterio es verificado por el comparador cmp

   
   
}
