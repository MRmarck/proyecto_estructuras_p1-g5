/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDAs;


import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 *
 * @author LAPTOP
 * @param <E>
 */
public class ArrayList<E> implements List<E> {
    private int capacidad = 100;
    private E elements[] = null;
    private int tamaño_efectivo = 0;
    public ArrayList() {
        this.elements = (E[]) new Object[capacidad];
    }
    private boolean isFull() {
        return elements.length == tamaño_efectivo;
    }
    private void addCapacity() {
        E[] temporal = (E[]) new Object[capacidad + 100];
        for (int i = 0; i < elements.length; i++) {
            temporal[i] = elements[i];
        }
        elements = temporal;
        capacidad = capacidad + 100;
    }
    @Override
    public boolean addFirst(E e) {
        if (e == null) {
            return false;
        } else {
            if (isFull()) {
                addCapacity();
            }
            if (tamaño_efectivo == 0) {
                elements[0] = e;
                tamaño_efectivo++;

                return true;
            } else {

                for (int i = tamaño_efectivo - 1; i >= 0; i--) {
                    elements[i + 1] = elements[i];

                    if (i == 0) {
                        elements[0] = e;
                        tamaño_efectivo++;

                        return true;

                    }

                }

            }
        }
        return false;

    }
    @Override
    public boolean addLast(E e) {
        if (e != null) {
            if (isFull()) {
                addCapacity();
            }
            elements[tamaño_efectivo] = e;
            tamaño_efectivo++;

            return true;
        } else {
            return false;
        }

    }
    @Override
    public E removeFirst() {
        E primero = elements[0];
        if (elements[0] != null) {
            for (int i = 0; i < tamaño_efectivo - 1; i++) {
                elements[i] = elements[i + 1];
            }
            elements[tamaño_efectivo] = null;
            tamaño_efectivo--;

            return primero;
        }
        return null;
    }
    @Override
    public E removeLast() {
        E ultimo = elements[tamaño_efectivo - 1];
        elements[tamaño_efectivo - 1] = null;
        tamaño_efectivo--;

        return ultimo;
    }
    @Override
    public int size() {
        return tamaño_efectivo;
    }
    @Override
    public boolean isEmpty() {
        return tamaño_efectivo == 0;
    }
    @Override
    public void clear() {
        E[] temporal = (E[]) new Object[capacidad];
        elements = temporal;
        tamaño_efectivo = 0;
    }
    @Override
    public void add(int index, E elemento) {
        if (elemento != null && index <= tamaño_efectivo - 1) {
                for (int i = 0; i <= tamaño_efectivo - 1; i++) {
                    if (i == index) {
                        for (int e = tamaño_efectivo - 1; e >= index; e--) {
                            elements[e + 1] = elements[e];
                        }
                        elements[index] = elemento;
                        tamaño_efectivo++;

                    
                }

            }
        }
    }
    @Override
    public E remove(int index) {
        if (index >= tamaño_efectivo) {
            return null;
        } else {

            for (int i = 0; i <= tamaño_efectivo - 1; i++) {

                if (i == index) {
                    E el = elements[index];

                    for (int e = i; e <= tamaño_efectivo - 1; e++) {

                        elements[e] = elements[e + 1];
                        if (e == tamaño_efectivo - 1) {
                            elements[e] = null;
                            tamaño_efectivo--;

                            return el;
                        }
                    }
                }

            }
        }
        return null;

    }
    @Override
    public E get(int index) {
        if (index < capacidad - 1) {
            for (int i = 0; i <= tamaño_efectivo - 1; i++) {
                if (index == i) {
                    return elements[index];
                }
            }
        } else {
            return null;
        }
        return null;
    }
    @Override
    public E set(int index, E element) {
        if (element != null) {
            if (index <= tamaño_efectivo - 1) {
                elements[index] = element;
                return element;
            }
            return null;
        }
        return null;
    }
    @Override
    public String toString() {
        StringBuilder bld = new StringBuilder();
        for (int i = 0; i <= capacidad - 1; i++) {
            if (elements[i] != null) {

                String prb = "El numero en la posicion" + i + "es" + elements[i] + "\n";
                bld.append(prb);

            }
        }
        return bld.toString();
    }
    @Override
    public Iterator<E> iterator() {
        Iterator<E> it = new Iterator<E>() {
            int c = 0;

            @Override
            public boolean hasNext() {
                return c < tamaño_efectivo;
            }

            @Override
            public E next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }

                E element = elements[c];
                c++;
                return element;

            }
        };
        return it;
    }
}
