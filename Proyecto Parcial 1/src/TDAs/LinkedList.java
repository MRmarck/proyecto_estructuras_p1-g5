/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDAs;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList<E> implements List<E> {
    private NodeList<E> header;
    private NodeList<E> last;
    private int tamano;
    public LinkedList() {
        header = null;
        last = null;
        tamano = 0;
    }
    public NodeList<E> getHeader() {
        return header;
    }
    public void setHeader(NodeList<E> header) {
        this.header = header;
    }
    public NodeList<E> getLast() {
        return last;
    }
    public void setLast(NodeList<E> last) {
        this.last = last;
    }
    @Override
    public boolean addFirst(E e) {
        NodeList<E> nuevo = new NodeList<>(e);
        if (e != null) {
            if (isEmpty()) {
                nuevo.setNext(header);
                setHeader(nuevo);
                setLast(nuevo);
                tamano++;
            } else {
                nuevo.setNext(header);
                setHeader(nuevo);
                tamano++;
            }
            return true;

        } else {

            return false;
        }
    }
    @Override
    public boolean addLast(E e) {
        NodeList<E> nuevo = new NodeList<>(e);
        if (e == null) {
            return false;
        }
        if (isEmpty()) {
            nuevo.setNext(header);
            setHeader(nuevo);
            setLast(nuevo);
            tamano++;
        } else {
            last.setNext(nuevo);
            setLast(nuevo);
            nuevo.setNext(null);
            tamano++;
        }
        return true;
    }
    @Override
    public E removeFirst() {
        if (isEmpty()) {
            return null;
        } else {
            E eliminado = getHeader().getContent();
            setHeader(header.getNext());
            tamano--;
            return eliminado;
        }
    }
    @Override
    public E removeLast() {
        if (isEmpty()) {
            return null;
        } else {
            E eliminado = getLast().getContent();
            NodeList<E> n;
            int v = 0;
            for (n = header; n != null; n = n.getNext()) {
                v++;
                if (tamano == 1) {
                    setHeader(null);
                    setLast(null);
                    tamano--;
                } else {
                    if (v == tamano - 1) {
                        setLast(n);
                        n.setNext(null);
                        tamano--;
                    }
                }
            }
            return eliminado;
        }
    }
    @Override
    public int size() {
        return tamano;
    }
    @Override
    public boolean isEmpty() {
        return header == null;
    }
    @Override
    public void clear() {
        setHeader(null);
        setLast(null);
    }
    @Override
    public void add(int index, E element) {
        if (element != null && index <= tamano) {
            if (index == 0) {
                addFirst(element);
            } else if (index == tamano) {
                addLast(element);
            } else {
                NodeList<E> n;
                int v = 0;
                NodeList<E> nuevo = new NodeList<>(element);
                for (n = header; n != null; n = n.getNext()) {
                    if (v == index - 1) {
                        nuevo.setNext(n.getNext());
                        n.setNext(nuevo);
                        tamano++;
                    }
                    v++;
                }
            }
        }
    }
    @Override
    public E remove(int index) {
        if (index < tamano) {
            NodeList<E> n;
            int v = 0;
            if (index == 0) {
                removeFirst();
            }
            if (index == tamano - 1) {
                removeLast();
            }
            for (n = header; n != null; n = n.getNext()) {

                if (index - 1 == v) {
                    E o = n.getNext().getContent();
                    n.setNext(n.getNext().getNext());
                    tamano--;
                    return o;
                }
                v++;
            }
        } else {
            return null;
        }
        return null;
    }
    @Override
    public E get(int index) {
        if (index < tamano) {
            NodeList<E> n;
            int v = 0;
            for (n = header; n != null; n = n.getNext()) {
                if (v == index) {
                    return (n.getContent());
                }
                v++;
            }
        }
        return null;
    }
    @Override
    public E set(int index, E element) {
        if (index < tamano) {
            NodeList<E> n;
            int v = 0;
            for (n = header; n != null; n = n.getNext()) {
                if (v == index) {
                    n.setContent(element);
                    return (n.getContent());
                }
                v++;
            }
        }
        return null;
    }
    @Override
    public String toString() {
        StringBuilder bld = new StringBuilder();
        NodeList<E> n;
        for (n = header; n != null; n = n.getNext()) {
            bld.append(n.getContent().toString());
        }
        return bld.toString();
    }
    @Override
    public Iterator<E> iterator() {
        Iterator<E> it = new Iterator<E>() {
            NodeList<E> n = header;

            @Override
            public boolean hasNext() {
                return n != null;
            }

            @Override
            public E next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }

                E mandar = n.getContent();
                n = n.getNext();
                return mandar;

            }
        };
        return it;
    }

}
